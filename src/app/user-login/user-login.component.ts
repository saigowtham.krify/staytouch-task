import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  loginForm:FormGroup;
  submitted=false;

  constructor(private formBuilder: FormBuilder,public route:Router) { }

  ngOnInit(): void {

    this.loginForm = this.formBuilder.group({
      userName: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(5)]],
      
  });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  userLogin=()=> {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }else{
      let userName=this.loginForm.value.userName;
      let password=this.loginForm.value.password;
      if(userName==='admin' && password==='admin'){
        localStorage.setItem('loginStatus','true')
        alert("You have Successfully logged in")
        this.route.navigate(['/meetings'])
      }else{
        alert("Login failed")
        this.route.navigate([''])
      }
      
    }
  }

}
