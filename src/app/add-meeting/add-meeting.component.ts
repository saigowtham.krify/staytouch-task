import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomServiceService } from '../services/custom-service.service';
import { Router } from '@angular/router';
import { SearchCountryField, CountryISO } from 'ngx-intl-tel-input';
@Component({
  selector: 'app-add-meeting',
  templateUrl: './add-meeting.component.html',
  styleUrls: ['./add-meeting.component.css']
})
export class AddMeetingComponent implements OnInit {

  meetingForm:FormGroup;
  submitted=false;
  separateDialCode = false;
  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
  constructor(private formBuilder: FormBuilder,public service:CustomServiceService,public route:Router) { }

  ngOnInit(): void {

    this.meetingForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName:[''],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required, Validators.minLength(10)]],
      address:[''],
      time:['', [Validators.required]]
  });
  }

  // convenience getter for easy access to form fields
  get f() { return this.meetingForm.controls; }

  createMeeting=()=> {
    this.submitted = true;
    // stop here if form is invalid
    if (this.meetingForm.invalid) {
        return;
    }else{
     let status= this.service.addMeeting(this.meetingForm.value)
     if(status){
       alert("Meeting Created Successfully")
      this.route.navigate(['/meetings'])
     }
      
    }
  }

}
