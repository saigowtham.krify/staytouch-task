export interface Meeting{
    firstName:string;
    lastName:string;
    email:string;
    phoneNumber:any;
    address:string;
    time:string
    id:string
}