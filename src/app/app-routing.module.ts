import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { from } from 'rxjs';
import { UserLoginComponent } from './user-login/user-login.component';
import { AddMeetingComponent } from './add-meeting/add-meeting.component';
import { EditMeetingComponent } from './edit-meeting/edit-meeting.component';
import { MeetingListComponent } from './meeting-list/meeting-list.component';
import {AuthGuardService} from './services/auth-guard.service'

const routes: Routes = [{path:'',component:UserLoginComponent},{path:'meetings',component:MeetingListComponent,canActivate:[AuthGuardService]},{path:'createMeet',component:AddMeetingComponent,canActivate:[AuthGuardService]},{path:'meetEdit/:id',component:EditMeetingComponent,canActivate:[AuthGuardService]}];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
