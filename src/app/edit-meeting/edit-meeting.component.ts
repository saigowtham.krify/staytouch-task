import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,ActivatedRoute} from '@angular/router'
import { CustomServiceService } from '../services/custom-service.service';
import { Meeting } from '../model/schema';
import { SearchCountryField, CountryISO } from 'ngx-intl-tel-input';
@Component({
  selector: 'app-edit-meeting',
  templateUrl: './edit-meeting.component.html',
  styleUrls: ['./edit-meeting.component.css']
})
export class EditMeetingComponent implements OnInit {

  editMeetingForm:FormGroup;
  submitted=false;
  meetings:Meeting[]=[];
  editData:any
  param:string
  separateDialCode = false;
  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [CountryISO.UnitedStates, CountryISO.UnitedKingdom];
  constructor(private formBuilder: FormBuilder,public activeRoute:ActivatedRoute,public service: CustomServiceService,public route:Router) { }

  ngOnInit(): void {
  
     this.param=this.activeRoute.snapshot.params.id;
    this.meetings = this.service.getMeetings();
    this.editData=this.meetings.find(p => p.id==this.param);
    this.editMeetingForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName:[''],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required, Validators.minLength(10)]],
      address:[],
      time:['', [Validators.required]]

      
  });
  }
  
  

  // convenience getter for easy access to form fields
  get f() { return this.editMeetingForm.controls; }

  editMeet=()=> {
    this.submitted = true;

    // stop here if form is invalid
    if (this.editMeetingForm.invalid) {
        return;
    }else{
      let id=this.activeRoute.snapshot.params.id;
      let status=this.service.editMeeting(this.editMeetingForm.value,id)
      if(status){
        alert("Meeting Data Successfully Updated")
        this.route.navigate(['/meetings'])
      }
      
    }
  }






}
