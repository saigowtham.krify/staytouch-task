import { Component, OnInit } from '@angular/core';
import { CustomServiceService } from '../services/custom-service.service';
import { Meeting } from '../model/schema';
import { Router,ActivatedRoute} from '@angular/router';
import {  Subject } from 'rxjs';

@Component({
  selector: 'app-meeting-list',
  templateUrl: './meeting-list.component.html',
  styleUrls: ['./meeting-list.component.css']
})
export class MeetingListComponent implements OnInit {

  meetings:Meeting[]=[];
  title = 'datatables';
  dtOptions: DataTables.Settings = {};
  dtTrigger = new Subject<any>();

  constructor(public service: CustomServiceService,private route:Router) {
    
  }
  ngOnInit(): void {

    

    // calling getMeeting service 
    this.meetings = this.service.getMeetings();

    //for pagination
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      processing: true,
      lengthChange:false,
      
    };
  }
  editMeet=(data)=>{
    let id=0
    console.log(data)
    if(data.id){
      id=data.id;
    }
    this.route.navigate([`/meetEdit/${id}`])
    
  }

// calling deleteMeeting service 
  deleteMeet=(data)=>{
    this.meetings=this.service.deleteMeeting(data);
    alert("Record have been removed successfully")
  }

  logOut=()=>{
    localStorage.removeItem('loginStatus')
    localStorage.removeItem('meetings')
    alert("Are you sure you want to log out?")
    this.route.navigate([''])
  }

}
