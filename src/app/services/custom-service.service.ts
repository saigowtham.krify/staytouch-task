import { Injectable } from '@angular/core';
import { Meeting } from '../model/schema';

@Injectable({
  providedIn: 'root'
})
export class CustomServiceService {
  meetings:Meeting[]=[];
  constructor() { }


  // geting meetings data from local storage
  getMeetings=()=> {
    if(localStorage.getItem('meetings') === null) {
      this.meetings = [];
    } else {
      this.meetings = JSON.parse(localStorage.getItem('meetings'));
    }
    return this.meetings;
  }


  // adding meetings from local storage
  addMeeting=(meeting: Meeting)=> {
    let id= Math.random().toString(36).substr(2, 9);
    this.meetings.push(meeting);
    let meetings = [];
    if(localStorage.getItem('meetings') === null) {
      meetings = [];
      meeting.id=id;
      meeting.phoneNumber = meeting.phoneNumber.e164Number
      meetings.push(meeting);
      localStorage.setItem('meetings', JSON.stringify(meetings));
    } else {
      meeting.id=id;
      meeting.phoneNumber = meeting.phoneNumber.e164Number
      meetings = JSON.parse(localStorage.getItem('meetings'));
      meetings.push(meeting); 
      localStorage.setItem('meetings', JSON.stringify(meetings));
      
    }
    
    return 1;
  }
//  updating meeting data from local storage
  editMeeting=(meeting: Meeting,id)=>{
   let getAllMeetings=this.getMeetings()
    for(let i = 0; i <getAllMeetings.length; i++) {
      if(getAllMeetings[i].id ==id) {
        meeting.id=id;
        meeting.phoneNumber = meeting.phoneNumber.e164Number
        getAllMeetings[i] = meeting;
     }
  }
  localStorage.setItem('meetings', JSON.stringify(getAllMeetings));
  return 1;
}

// deleteing meeting data from local storage
deleteMeeting=(meeting: Meeting)=>{
  let getAllMeetings=this.getMeetings()
    for(let i = 0; i <getAllMeetings.length; i++) {
      if(getAllMeetings[i].id ==meeting.id) {
       let   updatedMeetings = getAllMeetings.splice(i, 1); 
         
        
     }
  }
  localStorage.setItem('meetings', JSON.stringify(getAllMeetings));
return getAllMeetings;
}

}
